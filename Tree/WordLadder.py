import copy
class Solution:

    def solve(self, begin, end, l):
        l.add(begin)
        l.add(end)
        # final result
        res = []

        # hash map, key is string, value is tuple (depth, list of ancestors)
        m = {}

        # marking visited palces
        visited = set()

        # preferably a deque
        myset = []

        # put the first item in hash map m

        m[begin] = (0, [])

        # Node that we've visited
        visited.add(begin)
        myset.append(begin)

        final_depth = 0
        while len(myset) != 0:
            # pop head
            item = myset.pop(0)
            # find all neighbor of items
            kids = Solution.get_neighbor(item, l)

            for k in kids:
                if k not in visited:
                    visited.add(k)
                    if k == end:
                        # find a match
                        final_depth, _ = (m[item])
                        break;
                    else:
                        myset.append(k)
                        depth, traces = m[item]
                        new_traces = copy.deepcopy(traces)
                        new_traces.append(item)
                        m[k] = (depth+1, new_traces)

        Solution.dfs(begin, end, 0, final_depth+1, res, [], l)
        return res

    @staticmethod
    def get_neighbor(item, l):
        kid = []
        for i in l:
            if Solution.check_word(item, i):
                kid.append(i)
        return kid

    @staticmethod
    def check_word(w1, w2):
        count = 0
        for i in range(len(w1)):
            if w1[i] != w2[i]:
                count += 1
            if count > 1:
                return False
        return count == 1

    @staticmethod
    def dfs(root, target, depth, maxi, lres, ltemp, li):
        if depth == maxi:
            if root == target:
                n = copy.deepcopy(ltemp)
                n.append(root)
                lres.append(copy.deepcopy(n))
        else:
            n = copy.deepcopy(ltemp)
            n.append(root)
            kids = Solution.get_neighbor(root, li)
            for k in kids:
                Solution.dfs(k, target, depth + 1, maxi, lres, n, li)


if __name__=="__main__":
    l = set(["ted", "tex", "red", "tax", "tad", "den", "rex", "pee"])
    begin = "red"
    end = "tax"

    s = Solution()
    res = s.solve("red", "tax", l)

    for i in res:
        for j in i:
            print j
        print "\n"

