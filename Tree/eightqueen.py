import copy
import numpy as np


class Solution(object):
    def solveNQueens(self, n):
        """
        :type n: int
        :rtype: List[List[str]]
        """
        fr = [0]
        Solution.count([], 0, n, fr)
        return fr[0]

    @staticmethod
    def simpleCheck(l, x, y):
        for a,b in l:
            if a == x:
                return False
            if abs(a - x) == abs(b - y):
                return False
        return True

    @staticmethod
    def mark(m, N, x, y):
        for i in range(N):
            m[x][i] = -1
            m[i][y] = -1
        for i in range(1, N-y):
            if x + i < N:
                m[x+i][y+i] = -1
            if x - i >= 0:
                m[x-i][y+i] = -1

    @staticmethod
    def find(l, y, N, fr):
        if y == N:
            res = []
            for a in range(N):
                s = ""
                for b in range(N):
                    if (b, a) in l:
                        s += "Q"
                    else:
                        s += "."
                res.append(s)
            fr.append(res)

        else:
            for i in range(N):
                if Solution.simpleCheck(l, i, y):
                    cs = copy.deepcopy(l)
                    cs.append((i, y))
                    Solution.find(cs, y+1, N, fr)

    @staticmethod
    def count(l, y, N, sum):
        if y == N:
            sum[0] += 1
        else:
            for i in range(N):
                if Solution.simpleCheck(l, i, y):
                    cs = copy.deepcopy(l)
                    cs.append((i, y))
                    Solution.count(cs, y + 1, N, sum)

    # Why do I need to make deepcopy of s and m ?
    # pass by ref so changes done accumulates.
    # Why call solution.find instead of return solution.find?? If you hit return you never back track.


if __name__=="__main__":
    s = Solution()
    sum = s.solveNQueens(4)
    print sum[0]



