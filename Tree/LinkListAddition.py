# Opps this one is totally wrong lol
class Node:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    @staticmethod
    def partial_sum(l1, l2):
        if l1.next is None and l2.next is None:
            my_sum = l1.val + l2.val
            return my_sum / 10, Node(my_sum % 10)
        elif l1.next is None and l2.next is not None:
            carry, node = Solution.partial_sum(l1, l2.next)
            my_sum = carry + l2.val
            temp = Node(my_sum % 10)
            temp.next = node
            return my_sum / 10, temp
        elif l1.next is not None and l2.next is None:
            carry, node = Solution.partial_sum(l1.next, l2)
            my_sum = carry + l1.val
            temp = Node(my_sum % 10)
            temp.next = node
            return my_sum / 10, temp
        else:
            carry, node = Solution.partial_sum(l1.next, l2.next)
            my_sum = carry + l1.val + l2.val
            temp = Node(my_sum % 10)
            temp.next = node
            return my_sum / 10, temp

    @staticmethod
    def add(l1, l2):
        carry, node = Solution.partial_sum(l1, l2)
        if carry > 0:
            head = Node(1)
            head.next = node
            return head
        else:
            return node

    @staticmethod
    def display(n):
        if n is None:
            print("\n")
        else:
            print n.val
            Solution.display(n.next)


if __name__=="__main__":
    a = Node(1)
    b = Node(2)
    b.next = Node(3)
    a.next = b

    c = Node(9)
    c.next = Node(9)

    Solution.display(a)

    Solution.display(c)

    n = Solution.add(a, c)
    Solution.display(n)