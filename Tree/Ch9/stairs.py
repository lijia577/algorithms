class Solution(object):
    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        """
        # n is number of stair

        m = [-1] * (n + 1)
        m[0] = 1
        return Solution.hp(n, m)

    @staticmethod
    def hp(n, m):
        if n < 0:
            return 0
        else:
            if m[n] == -1:
                m[n] = Solution.hp(n - 1, m) + Solution.hp(n - 2, m)
                return m[n]
            else:
                return m[n]
