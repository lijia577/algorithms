# -*- coding: utf-8 -*-
"""
correctness:
    x ... y
    if a[x] + a[y] < target, means px++, this is because y is the largest. Also, you don’t ever need to consider
    x because y is the largest number in array, x + any number smaller than y is still smaller than target.
    (That is, you should never consider x again).

    if a[x] + a[y] > target, means py- -. This is because x is the smallest. So you can only decrease y.
    Also, you should never consider y again because y + another number greater than x is still larger
    than the target.

    X, x . . . . . . . y, Y
    x + y > target, you must decrease y because X is to the left of X, you must have considered it already
    previously.

Complexity:
    The complexity is sorting, O( n(log n) ) + O(n) = O(nlog(n))


Try:
    Solving this using hash map gives you the optimal performance.

Tried:
    The idea is to use a dict, for the number at index i, putting the value it is expected in the key. Therefore,
    once there is a hit, you know some number is expecting you to come. Hence O(n) complexity.

"""


class Solution(object):
    @staticmethod
    def two_sum(nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        nums = sorted(nums)
        px = 0
        py = len(nums) - 1
        while True:
            if px == py:
                return -1, -1
            elif nums[px] + nums[py] < target:
                px += 1
            elif nums[px] + nums[py] > target:
                py -= 1
            else:
                return px, py

    @staticmethod
    def better_two_sum(nums, target):
        d = {}
        for i in range(len(nums)):
            try:
                index = d[nums[i]]
                return i, index
            except KeyError:
                d[target - nums[i]] = i
        return -1, -1


if __name__ == "__main__":
    arr = [2, 99, 7, 12, 1, 3, 15]
    s = Solution()
    x, y = s.better_two_sum(arr, 18)
    print(x)
    print(y)
