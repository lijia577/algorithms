"""
    Given a tree, save the root in list 1, save all children of root in list 2, save all grand children of root
    in list 3 and so on...
    Return a list contain list 1, 2, 3 ...

    Traverse the tree in a BFS manner. Using None as a dummy node to mark the end of each level.
"""

import copy


class Node:
    def __init__(self, value):
        self.kids = []
        self.value = value


def foo(root):
    # result is a list of list
    result = []
    l = []
    queue = [root, None]
    while len(queue) != 0:
        item = queue.pop(0)
        if item is None:
            result.append(copy.deepcopy(l))
            l = []
            if len(queue) != 0:
                queue.append(None)
        else:
            l.append(item)
            for kid in item.kids:
                queue.append(kid)
    return result


if __name__ == "__main__":
    a = Node(1)
    b = Node(2)
    c = Node(3)
    d = Node(4)
    e = Node(5)

    a.kids.append(b)
    b.kids.append(c)

    c.kids.append(d)
    d.kids.append(e)

    ll = foo(a)

    for x in ll:
        for y in x:
            print y.value
        print "\n"


