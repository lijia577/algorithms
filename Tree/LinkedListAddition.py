class ListNode:
    def __init__(self, val):
        self.val = val
        self.next = None


class Solution:
    @staticmethod
    def reverse(l):
        if l is None or l.next is None:
            return l
        else:
            temp = Solution.reverse(l.next)
            handle = temp
            while handle.next is not None:
                handle = handle.next
            handle.next = l
            l.next = None
            return temp

    @staticmethod
    def display(n):
        if n is None:
            print("\n")
        else:
            print n.val
            Solution.display(n.next)

    # This one uses tail recursion, two pointer
    @staticmethod
    def reverse(cur, prev):
        if cur.next is None:
            cur.next = prev
            return cur
        else:
            next = cur.next
            cur.next = prev
            return Solution.reverse(next, cur)

    # Almost equivalently, you would know the iterative approach
    @staticmethod
    def reverse_it(head):
        cur = head
        prev = None
        while cur is not None:
            nxt = cur.next
            cur.next = prev
            prev = cur
            cur = nxt
        return prev

    def addition(self, l1, l2):
        r1 = Solution.reverse_it(l1)
        r2 = Solution.reverse_it(l2)
        res = Solution.addition_helper(r1, r2, 0)
        ress = Solution.reverse_it(res)
        return ress

    @staticmethod
    def addition_helper(l1, l2, carry):
        if l1 is None and l2 is None:
            if carry > 0:
                return ListNode(carry)
            else:
                return None
        elif l1 is not None and l2 is not None:
            mysum = l1.val + l2.val + carry
            res = ListNode(mysum % 10)
            carry = mysum / 10
            temp = Solution.addition_helper(l1.next, l2. next, carry)
            res.next = temp
            return res
        elif l1 is None and l2 is not None:
            mysum = l2.val + carry
            res = ListNode(mysum % 10)
            carry = mysum / 10
            temp = Solution.addition_helper(l1, l2.next, carry)
            res.next = temp
            return res
        else:
            mysum = l1.val + carry
            res = ListNode(mysum % 10)
            carry = mysum / 10
            temp = Solution.addition_helper(l1.next, l2, carry)
            res.next = temp
            return res

if __name__=="__main__":
    a = ListNode(2)
    b = ListNode(4)
    a.next = b
    b.next = ListNode(3)

    c = ListNode(5)
    d = ListNode(6)
    c.next = d
    d.next = ListNode(4)


    s = Solution()
    res = s.addition(a, c)
    Solution.display(res)

