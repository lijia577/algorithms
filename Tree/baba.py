
def solution(begin, end, l):
    l.add(begin)
    baba = {}
    open = [end]
    while len(open) > 0:
        open_2 = set()
        for candidate in open:
            # find candidate's baba
            for i in range(0, len(candidate)):
                partA = candidate[:i]
                partB = candidate[i + 1:]
                for partC in "abcdefghijklmnopqrstuvwxyz":
                    temp = partA + partC + partB
                    if temp in l:
                        open_2.add(temp)
                        try:
                            baba[temp].add(candidate)
                        except KeyError:
                            baba[temp] = set([candidate])

        if begin in open_2:
            break
        l = l - open_2
        open = open_2

    return build_path(baba, begin, end)

def build_path(baba, current, end):
    if current == end:
        return [[current]]

    result = []
    if current not in baba:
        return []

    for candidate in baba[current]:
        jt = build_path(baba, candidate, end)
        for path in jt:
            path.insert(0, current)
            result.append(path)
    return result


l = set(["ted", "tex", "red", "tax", "tad", "den", "rex", "pee"])
begin = "red"
end = "tax"

print solution(begin, end, l )