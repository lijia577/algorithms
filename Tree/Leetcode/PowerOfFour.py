"""
    This is a variation of power of two. First, you check if it is power of two
    Then, you check if the 1 bit is appearing at the desired position and figuring out the position.
"""

class Solution(object):
    def isPowerOfFour(self, n):
        if n <= 0:
            return False
        else:
            if n == 1 or (n - 1) & n == 0:
                return n & 0xAAAAAAAA == 0
            else:
                return False


