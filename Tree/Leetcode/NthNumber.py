"""
Write a function to return the nth number of sequence 1 2 3 4 5 6 7 8 9 10 11
given 1 return 1, given 3 return 3, given 10th, however, return 1, given 11, return 0

convert list into all strings, then return nth. But this is bad,
given a number n
if n < 10: return n
if n > 10 and < 100:   base: 9,  skip over: (n-9)/2,  return the (n-9)%2  th digit (zero indexed)
if n > 100 and < 1000: base: 99, skip over: (n-99)/3, return the (n-99)%3 th digit (zero indexed)

if digit = 2:          base: 10^(digit-1) - 1, skip over (n-base)/digit, return (n-base)%digit th digit, zero based

101112

11
base = 9
skip = 2/2 = 1
d = 1 % 2 = 1
10 + skip = 10,  1 th

11
base = 10
skip = 1/2  = 0
d = 1%2 = 1
10 + skip = 10, 1 th

190
base = 10 + 90 * 2 = 190
skip = 0/3 = 0
d = 0%3 = 0
1



get nth_of_seq(n, digit):
    base = 10 + 9 * pow(10,digit) * digit
    skip = (n-base)/digit
    d = (n-base)%digit
    return nth(d, base + skip)

get nth(n, num):
    x = str(num)
    return int(num[n])


         n,  1,     9
getDigit(n, level, rhs):
    if n < rhs:
        return level
    else:
        return getDigit(n - rhs, level + 1, rhs * 10 * (level+1))

getBase(digit, ps ):
    if (digit == 1):
        return 10 + ps
    else:
        return getBase(digit-1, 9 * pow(10, digit-1), * digit)

9 + 180 = 189 190
how many numbers are there that are 1 digit? 10 - 1 - prevSum = 9 * 10^0 * 1      = 10
how many numbers are there that are 2 digit? 100 - 1 - prevSum = 9 * 10^1 * 2      = 180
                                    3 digit? 1000 - 1 - prevSum = 9* 10^2 * 3      = 2700
                                    4 digit? 10000 - 1 - prevSum = 9* 10^3 * 4



15th: 15 - 9 = 6, skip over number 6/2 = 3 leftover 0, return 9 + 3 = 12, 12, 0th 1
14th: 14 - 9 = 5, skip over number 5/2 = 2 leftover 1, return 9 + 2 = 11, 11, 1th 1

101112
"""
class Solution(object):
    def findNthDigit(self, n):
        if n <= 9:
            return n
        else:
            digit = self.getDigit(n, 1, 10)
            skip  = self.getBase(digit, 0)
            jump  = (n - skip) / digit
            d     = (n - skip) % digit
            #print(digit, skip, jump, d)
            return self.nth(d, pow(10,digit) + jump)

    # get the nth digit of integer num, O(num), num ~ n, log(n)
    def nth(self, n, num):
        x = str(num)
        return int(x[n])

    # this is log(n) base 10
    def getDigit(self, n, level, rhs):
        if n < rhs:
            return level
        else:
            return self.getDigit(n - rhs, level + 1, 9 * pow(10,level) * (level + 1))

    # same above
    def getBase(self, digit, ps):
        if digit == 1:
            return 10 + ps
        else:
            return self.getBase(digit-1, 9 * pow(10, digit-1) * digit + ps)

    def right(self, n):
        i = 1;
        while n > len(str(i)):
            n -= len(str(i))
            i += 1
        return int(str(i)[n-1])

if __name__=="__main__":
    s = Solution()
    #print(s.findNthDigit(192))
    for i in range(1, 10000000):
        if s.right(i) != s.findNthDigit(i):
            print(s.right(i), s.findNthDigit(i), i)
            print("error")