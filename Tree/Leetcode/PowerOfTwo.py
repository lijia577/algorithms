"""
    Power of Two:
    The main idea is: n & (n - 1)

    Check special case 0 and 1

    Accepted.
"""

class Solution(object):
    def isPowerOfTwo(self, n):
        if n == 0:
            return False
        elif n == 1:
            return True
        else:
            return n & (n - 1) == 0

