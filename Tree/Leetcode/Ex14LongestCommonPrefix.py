"""
Write a function to find the longest common prefix string amongst an array of strings.

Easy question but do you test, test for special cases!!
"""
class Solution(object):
    def getCommonPrefix(self, a, b):
        # get the shortest len of two
        if len(a) < len(b):
            end = len(a)
        else:
            end = len(b)

        i = 0
        while i < end:
            if a[i] == b[i]:
                i += 1
            else:
                break
        if end == 0 or i == 0:
            return ""
        else:
            return a[:i]

    def longestCommonPrefix(self, strs):
        common = strs[0]
        for i in range(1, len(strs)):
            common = self.getCommonPrefix(common, strs[i])
        return common

if __name__ == "__main__":
    strs = ["123123123", "321", "123", "123123"]
    s = Solution()

    print(s.longestCommonPrefix(strs))
