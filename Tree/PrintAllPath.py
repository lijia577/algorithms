"""
    Almost anytime you need to operate on a list, you need to make a deep copy of it.
    Unless you are using it like a accumulator.
"""
import copy


class Node:
    def __init__(self, val):
        self.value = val
        self.adj = []


def dfs(root, target, depth, maxi, lres, ltemp):
    if depth == maxi:
        if root == target:
            n = copy.deepcopy(ltemp)
            n.append(root)
            lres.append(copy.deepcopy(n))
    else:
        n = copy.deepcopy(ltemp)
        n.append(root)
        for k in root.adj:
            dfs(k, target, depth+1, maxi, lres, n)

if __name__=="__main__":
    s = Node(0)
    a = Node(1)
    b = Node(2)
    c = Node(3)
    d = Node(4)
    t = Node(5)
    s.adj = [a, b]
    a.adj = [s, c]
    b.adj = [s, c, d]
    c.adj = [a, b, t]
    d.adj = [b, t]
    t.adj = [c, d]

    lres = []
    dfs(s, t, 0, 3, lres, [])

    for i in lres:
        for j in i:
            print j.value
        print "\n"
