"""
Basically, this is a decimal to 26-decimal problem, solve it using long division?
Nope! This is different. The Excel Header problem is not zero started. It starts at one.
Hence, if you use long division or try using % and / (which is sort of long division),
You will not get the desired results. You have to use subtraction kind of way.
"""
class Solution(object):
    # i is numbers that are used in previous level.
    # Given a number n, decide how many digit does its excel header form have. For example,
    # 52 is AZ, return 2
    # First level, has 26, second level, has 26 ^ 2, so, if n is beyond 26, subtract 26 from n
    # you should expect it not exceeding 26^2 if it is 2 level, otherwise, just repeat steps
    def getDigit(self, n, level, rhs):
        if n <= rhs:
            return level
        else:
            return self.getDigit(n - rhs, level + 1, pow(26, level+1))
    '''
    Checking: n = 26, i = 0, level = 1, rhs = 26
              n = 27, i = 26, level = 2, rhs = 26^2
              n = 25 + 26*26, i = 0, level = 1, rhs = 26
              n = ..          i = 26, level = 2, rhs = 26 + 26^2
              n =             i = 26 + 26^2 level = 3, rhs = ....
    '''
    def convert(self, n):
        d = self.getDigit(n, 1, 26)
        return self.helper(n, d, "")

    def helper(self, n, d, res):
        al = " ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        if d == 1:
            #print("n " , n)
            res += str(al[n])
            return res
        else:
            # count the digit at this level, store it in x
            x = n / (pow(26, d - 1))
            # check the module, since this is not zero based, for example
            # For 52, It should really be AZ, not B_, so check module, if
            # it is zero, you are overfilling this level, in other words,
            # you used this to make sure there is something left to fill
            # up other levels. Hence, other levels can't be zero .
            if n % (pow(26, d - 1)) == 0:
                x = x - 1
            # You substract from n the numbers you used letters to represent
            # All the others still needs to be filled in.
            r = n - x * (pow(26, d - 1))
            res += str(al[x])
            return self.helper(r, d - 1, res)
    """
    checking:
       52,  2,
       703, 3, ""
       x = 703/676 = 1
       r = 27
       res = "A"
       27, 2, "A"
       x = 27/26 = 1
       r = 27 %26 = 1
       1, 1, "AA"
       1, 0, "AAA"

       28, 2, "", 1
       x = 28/26 = 1
       r = 28%26 = 2
       res = "A"
       2, 1, "A"

       26
       AA, AB, AC = 26
       ..
       Z
       26 * 26 + 26 + 1 = 703

    """

    def convertToTitle(self, n):
        al = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        res = ""
        d = 0
        while n >= 26:
            d = n / 26
            r = n % 26
            res = al[r] + res
            n = d
        res = al[n] + res
        return res

'''
complexity:
 log(n) base 26, not very complex ;)
'''
if __name__=="__main__":
    s = Solution()
    for i in range(0, 1000):
        print(s.convert(i))

